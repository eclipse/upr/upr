/*******************************************************************************
 * Copyright (c) 2013 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/
package org.eclipse.upr.platform.java.cm2up.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gmt.modisco.java.emf.JavaPackage;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.modisco.infra.discovery.core.exception.DiscoveryException;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.util.UMLValidator;
import org.eclipse.upr.platform.java.cm2up.ResourceBasedCodeModel2UMLProfile;
import org.eclipse.upr.platform.java.code2cm.JavaDiscoverer;
import org.eclipse.upr.platform.trace.TracePackage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Alexander Bergmayr
 *
 */
public class TestJavaLibraries {
	
	
	/** default input for the UML profile discovery transformation */
	public final String jpModelPath = "pathmap://JAVA_PROFILE/javaProfile.uml"; 
	public final String jptModelPath = "pathmap://UML_LIBRARIES/JavaPrimitiveTypes.library.uml"; 
	public final String mcModelPath = "pathmap://UML_METAMODELS/UML.metamodel.uml";
	public final String uptModelPath = "pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml";
	public final String eptModelPath = "pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml";
	
	public final String codeModelDirectoryPath = "data/codemodel/";
	public final String cfgModelDirectoryPath = "data/cfg/";
	
	public final String umlProfileDirectoryPath = "data/umlprofile/";
	public final String umlProfilePathSegment = "_profile.profile.uml";
	public final String tracePathSegment = "_profile.trace.xmi";
	
	ResourceSet resourceSet = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		resourceSet = new ResourceSetImpl();
		
		// ensure that we can load CodeModels as well as UML Models / Profiles
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
	
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put("xmi", new XMIResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put("jcfg", new XMIResourceFactoryImpl());
		
		UMLPackage.eINSTANCE.eClass();
		JavaPackage.eINSTANCE.eClass();
		TracePackage.eINSTANCE.eClass();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testObjectify() throws ATLCoreException, IOException {
		String codeModelName = "objectify";
		String cfgModelName = "configuration";
		
		URI modelURI = URI.createFileURI(new File(codeModelDirectoryPath.concat(codeModelName.concat(".xmi"))).getAbsolutePath());
		Resource codeModel = resourceSet.getResource(modelURI, true);
		
		URI cfgModelURI = URI.createFileURI(new File(cfgModelDirectoryPath.concat(cfgModelName.concat(".jcfg"))).getAbsolutePath());
		Resource cfgModel = resourceSet.getResource(cfgModelURI, true);
	
		Resource umlProfile = runUMLProfileDiscoverer(codeModel, cfgModel, codeModelName, true);
	
		assertTrue("Validate the generated UML Profile", runValidation(umlProfile));
	}
	
	@Test
	public void testObjectifyFromCode() throws ATLCoreException, IOException, JavaModelException, DiscoveryException {
		String projectName = "objectify";
		String cfgModelName = "configuration";
		
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = workspaceRoot.getProject(projectName);
		
		IJavaProject javaProject = JavaCore.create(project);
		javaProject.open(new NullProgressMonitor());
		
		Resource codeModel = JavaDiscoverer.INSTANCE.runDiscovery(javaProject);
		
		URI cfgModelURI = URI.createFileURI(new File(cfgModelDirectoryPath.concat(cfgModelName.concat(".jcfg"))).getAbsolutePath());
		Resource cfgModel = resourceSet.getResource(cfgModelURI, true);
		
		Resource umlProfile = runUMLProfileDiscoverer(codeModel, cfgModel, projectName, true);
		
		assertTrue("Validate the generated UML Profile", runValidation(umlProfile));
	}
	
	private boolean runValidation(Resource umlProfile) {
		boolean isValid = false;
		Package pack = (Package) umlProfile.getContents().get(0);
		isValid = UMLValidator.INSTANCE.validatePackage(pack, new BasicDiagnostic(), new HashMap<Object, Object>());
		
		return isValid;
	}
	
	private Resource runUMLProfileDiscoverer(Resource codeModel, Resource cfgModel, String umlProfileName, boolean serialize) throws ATLCoreException, IOException {
		ResourceBasedCodeModel2UMLProfile runner = new ResourceBasedCodeModel2UMLProfile();
		runner.setUmlProfilePath(umlProfileDirectoryPath.concat(umlProfileName).concat(umlProfilePathSegment));
		runner.setTraceModelPath(umlProfileDirectoryPath.concat(umlProfileName).concat(tracePathSegment));
		runner.loadModels(codeModel, cfgModel, jpModelPath, jptModelPath, mcModelPath, uptModelPath, eptModelPath);
		runner.doCodeModel2UMLProfile(new NullProgressMonitor());
		if(serialize) runner.saveUMLProfileModel();
		
		return ((EMFModel) runner.getUpModel()).getResource();
	}

}
