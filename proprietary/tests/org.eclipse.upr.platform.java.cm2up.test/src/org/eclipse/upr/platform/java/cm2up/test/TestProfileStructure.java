/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

/**
 * 
 */
package org.eclipse.upr.platform.java.cm2up.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Package;
import org.eclipse.upr.platform.java.jumpcfg.util.JConfigurationUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Alexander Bergmayr
 *
 */
public class TestProfileStructure {

	ResourceSet resourceSet = null;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		resourceSet = new ResourceSetImpl();
		TestUtil.setUpResourceSet(resourceSet);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAnnotationDeclaration() throws ATLCoreException,
			IOException {
		String codeModelName = "annotation.declaration";

		Resource umlProfile = TestUtil.runUMLProfileDiscoverer(
				TestUtil.createCodeModelResource(codeModelName),
				JConfigurationUtil.getDefaultConfiguration()	,
				codeModelName, true);

		assertTrue("Validate the generated UML Profile",
				TestUtil.runValidation(umlProfile));

		assertTrue("Profile has been generated", umlProfile.getContents()
				.get(0) instanceof Profile);

		Profile profile = (Profile) umlProfile.getContents().get(0);

		List<String> packageNames = Arrays.asList("annotation", "declaration");
		List<String> stereotypeNames = Arrays.asList("MyAnnotation");

		assertTrue("Profile contains expected package(s)",
				UMLHelper.containsNamedElement(UMLHelper
						.getAllOwnedElementsByType(profile, Package.class),
						packageNames));
		assertTrue("Profile contains expected stereotype(s)",
				UMLHelper.containsNamedElement(UMLHelper
						.getAllOwnedElementsByType(profile, Stereotype.class),
						stereotypeNames));
		assertTrue("Profile contains required extensions",
				UMLHelper.getAllOwnedElementsByType(profile, Extension.class)
						.size() == 7);
	}

	@Test
	public void testAnnotationMemberDeclaration() throws ATLCoreException,
			IOException {
		String codeModelName = "annotationmember.declaration";
		String cfgModelName = "configuration";

		Resource umlProfile = TestUtil.runUMLProfileDiscoverer(
				TestUtil.createCodeModelResource(codeModelName),
				TestUtil.createConfigurationResource(cfgModelName),
				codeModelName, true);

		assertTrue("Validate the generated UML Profile",
				TestUtil.runValidation(umlProfile));

		assertTrue("Profile has been generated", umlProfile.getContents()
				.get(0) instanceof Profile);

		Profile profile = (Profile) umlProfile.getContents().get(0);

		List<String> memberNames = Arrays.asList("myStringMember",
				"myAnnotationMember", "myClassMember");

		assertTrue("Profile contains expected property(ies)",
				UMLHelper.containsNamedElement(UMLHelper
						.getAllOwnedElementsByType(profile, Property.class),
						memberNames));
		
		Property myAnnotationMember = UMLHelper.getElementByName(
				UMLHelper.getAllOwnedElementsByType(profile, Property.class),
				"myAnnotationMember", Property.class);

		assertTrue(
				"Properties of complex type must have aggregration set to 'composite'",
				myAnnotationMember.isComposite());

		Property myClassMember = UMLHelper.getElementByName(
				UMLHelper.getAllOwnedElementsByType(profile, Property.class),
				"myClassMember", Property.class);

		// generally, annotation member declarations of complex type (including annotation)
		// need to translated into properties with aggregation set to 'composite'.
		// however annotation member declarations of type 'class' are an exception
		assertTrue(
				"Properties of type class must have aggregration set to 'none'",
				!myClassMember.isComposite());
	}

	@Test
	public void testTargetDeclaration() throws ATLCoreException, IOException {
		String codeModelName = "target.declaration";
		String cfgModelName = "configuration";

		Resource umlProfile = TestUtil.runUMLProfileDiscoverer(
				TestUtil.createCodeModelResource(codeModelName),
				TestUtil.createConfigurationResource(cfgModelName),
				codeModelName, true);

		assertTrue("Validate the generated UML Profile",
				TestUtil.runValidation(umlProfile));

		assertTrue("Profile has been generated", umlProfile.getContents()
				.get(0) instanceof Profile);

		Profile profile = (Profile) umlProfile.getContents().get(0);

		assertTrue("Profile contains required extensions",
				UMLHelper.getAllOwnedElementsByType(profile, Extension.class)
						.size() == 1);
		assertTrue("Profile contains required extensions", UMLHelper
				.getAllOwnedElementsByType(profile, Extension.class).get(0)
				.getMember("base_Type") != null);
		assertTrue("Profile contains required extensions", UMLHelper
				.getAllOwnedElementsByType(profile, Extension.class).get(0)
				.getMember("extension_Type") != null);
	}

}
