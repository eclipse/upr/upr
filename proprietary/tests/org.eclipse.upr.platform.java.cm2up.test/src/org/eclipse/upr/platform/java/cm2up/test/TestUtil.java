/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.cm2up.test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.gmt.modisco.java.emf.JavaPackage;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.eclipse.uml2.uml.util.UMLValidator;
import org.eclipse.upr.platform.java.cm2up.ResourceBasedCodeModel2UMLProfile;
import org.eclipse.upr.platform.trace.TracePackage;

public class TestUtil {
	
	/** default input for the UML profile discovery transformation */
	public static final String JP_Model_Path = "pathmap://JAVA_PROFILE/javaProfile.uml"; 
	public static final String JPT_Model_Path = "pathmap://UML_LIBRARIES/JavaPrimitiveTypes.library.uml"; 
	public static final String MC_Model_Path = "pathmap://UML_METAMODELS/UML.metamodel.uml";
	public static final String UPT_Model_Path = "pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml";
	public static final String EPT_Model_Path = "pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml";
	
	public static final String CODE_MODEL_DIRECTORY_PATH = "data/codemodel/";
	public static final String CFG_MODEL_DIRECTORY_PATH = "data/cfg/";
	
	public static final String UML_PROFILE_DIRECTORY_PATH = "data/umlprofile/";
	public static final String UML_PROFILE_PATH_SEGMENT = "_profile.profile.uml";
	public static final String TRACE_PATH_SEGMENT = "_profile.trace.xmi";
	
	public static final String CODE_MODEL_FILE_EXTENSION = "xmi";
	public static final String CFG_MODEL_FILE_EXTENSION = "jcfg";
	
	private static ResourceSet RESOURCE_SET;
	
	/**
	 * Helper for configuring the resource set.
	 * 
	 * @param resourceSet 
	 */
	public static void setUpResourceSet(ResourceSet resourceSet) {
		RESOURCE_SET = resourceSet;
		
		// ensure that we can load CodeModels as well as UML Models / Profiles
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put(UMLResource.FILE_EXTENSION, UMLResource.Factory.INSTANCE);
	
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put(CODE_MODEL_FILE_EXTENSION, new XMIResourceFactoryImpl());
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put(CFG_MODEL_FILE_EXTENSION, new XMIResourceFactoryImpl());
		
		UMLPackage.eINSTANCE.eClass();
		JavaPackage.eINSTANCE.eClass();
		TracePackage.eINSTANCE.eClass();
	}
	
	/**
	 * 
	 * @param codeModelName
	 * @return
	 */
	public static Resource createCodeModelResource(String codeModelName) {
		StringBuilder codeModelPath = new StringBuilder();
		codeModelPath.append(CODE_MODEL_DIRECTORY_PATH);
		codeModelPath.append(codeModelName);
		codeModelPath.append(".");
		codeModelPath.append(CODE_MODEL_FILE_EXTENSION);
		URI modelURI = URI.createFileURI(new File(codeModelPath.toString()).getAbsolutePath());
		return RESOURCE_SET.getResource(modelURI, true);		
	}
	
	/**
	 * 
	 * @param cfgModelName
	 * @return
	 */
	public static Resource createConfigurationResource(String cfgModelName) {
		StringBuilder codeModelPath = new StringBuilder();
		codeModelPath.append(CFG_MODEL_DIRECTORY_PATH);
		codeModelPath.append(cfgModelName);
		codeModelPath.append(".");
		codeModelPath.append(CFG_MODEL_FILE_EXTENSION);
		URI cfgModelURI = URI.createFileURI(new File(CFG_MODEL_DIRECTORY_PATH.concat(cfgModelName.concat(".jcfg"))).getAbsolutePath());
		return RESOURCE_SET.getResource(cfgModelURI, true);		
	}
	
	/**
	 * 
	 * @param umlProfile
	 * @return
	 */
	public static boolean runValidation(Resource umlProfile) {
		boolean isValid = false;
		Package pack = (Package) umlProfile.getContents().get(0);
		isValid = UMLValidator.INSTANCE.validatePackage(pack, new BasicDiagnostic(), new HashMap<Object, Object>());
		
		return isValid;
	}
	
	/**
	 * 
	 * @param codeModel
	 * @param cfgModel
	 * @param umlProfileName
	 * @param serialize
	 * @return
	 * @throws ATLCoreException
	 * @throws IOException
	 */
	public static Resource runUMLProfileDiscoverer(Resource codeModel, Resource cfgModel, String umlProfileName, boolean serialize) throws ATLCoreException, IOException {
		ResourceBasedCodeModel2UMLProfile runner = new ResourceBasedCodeModel2UMLProfile();
		runner.setUmlProfilePath(UML_PROFILE_DIRECTORY_PATH.concat(umlProfileName).concat(UML_PROFILE_PATH_SEGMENT));
		runner.setTraceModelPath(UML_PROFILE_DIRECTORY_PATH.concat(umlProfileName).concat(TRACE_PATH_SEGMENT));
		runner.loadModels(codeModel, cfgModel, JP_Model_Path, JPT_Model_Path, MC_Model_Path, UPT_Model_Path, EPT_Model_Path);
		runner.doCodeModel2UMLProfile(new NullProgressMonitor());
		if(serialize) runner.saveUMLProfileModel();
		
		return ((EMFModel) runner.getUpModel()).getResource();
	}
}
