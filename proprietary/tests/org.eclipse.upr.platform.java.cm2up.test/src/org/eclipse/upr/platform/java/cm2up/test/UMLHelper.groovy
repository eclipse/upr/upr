/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.cm2up.test

import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Profile
import org.eclipse.uml2.uml.Stereotype
import org.eclipse.uml2.uml.Package

class UMLHelper {
		
	public static <E> List<E> getAllOwnedElementsByType(Profile profile, Class<E> type) {		
		return (profile.allOwnedElements() as List).findAll { it.class.interfaces.contains(type)}
	}
	
	public static<E> E getElementByName(List<? extends NamedElement> namedElements, String element, Class<E> type) {
		return namedElements.find { it.name == element }
	}
	
	public static boolean containsNamedElement(List<? extends NamedElement> namedElements, List<String> elements) {
		def elementNames = (namedElements as List).collect {it.name}
		return  elementNames.containsAll(elements);
	}  

}
