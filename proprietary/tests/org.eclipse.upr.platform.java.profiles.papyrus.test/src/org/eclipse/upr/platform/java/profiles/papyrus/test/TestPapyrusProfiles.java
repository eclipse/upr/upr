/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.profiles.papyrus.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.papyrus.uml.extensionpoints.Registry;
import org.eclipse.papyrus.uml.extensionpoints.profile.IRegisteredProfile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPapyrusProfiles {

	Collection<String> uprProfiles;

	@Before
	public void setUp() throws Exception {
		uprProfiles = new ArrayList<String>(Arrays.asList("EclipseLink", "EJB3.0", "EJB3.2",
				"FindBugs", "Hibernate-Core", "Jackson", "Javax-Annotations",
				"Javax-CDI", "Javax-Inject", "Javax-Interceptor",
				"Javax-Validation", "Javax-WS", "Jaxb", "Jaxrs",
				"JBoss-Arquillian", "Java7-Annotations", "JPA2", "JUnit",
				"Objectify", "Spring-Bundle", "Struts-Bundle"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		List<IRegisteredProfile> registeredProfiles = Registry
				.getRegisteredProfiles();
		List<String> registeredUPRProfiles = new LinkedList<String>();

		for (IRegisteredProfile iRegisteredProfile : registeredProfiles) {
			if (iRegisteredProfile.getProvider()
					.equalsIgnoreCase("Eclipse UPR")) {
				registeredUPRProfiles.add(iRegisteredProfile.getName());
			}
		}
		
		assertTrue("Are all profiles registered?", uprProfiles.size() == registeredUPRProfiles.size());
		
		uprProfiles.removeAll(registeredUPRProfiles);
		assertTrue("Missing profiles: " + uprProfiles, uprProfiles.isEmpty());
	}

}
