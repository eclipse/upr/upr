/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.profiles.test;

import static org.eclipse.uml2.uml.UMLPlugin.getEPackageNsURIToProfileLocationMap;
import static org.eclipse.uml2.uml.resources.util.UMLResourcesUtil.init;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.Profile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestProfiles {

	Collection<String> uprProfiles;

	@Before
	public void setUp() throws Exception {
		uprProfiles = new ArrayList<String>(
				Arrays.asList(
						"http:///schemas/EclipseLink/_bwp8sBt1EeWb19NaJQmczQ/0",
						"http:///schemas/ejb30/_cWvFQBt1EeWb19NaJQmczQ/0",
						"http:///schemas/ejb32/_c7NrUBt1EeWb19NaJQmczQ/0",
						"http:///schemas/findBugs/__KplkBt0EeWb19NaJQmczQ/0",
						"http:///schemas/hibernatecore/_lQUHUBtwEeWZE8BV_IWn7Q/0",
						"http:///schemas/jacksonannotations/_tkL-ALAwEeSts9jnR_oQsA/0",
						"http:///schemas/javaxannotation/_xb5hIBt1EeWb19NaJQmczQ/0",
						"http:///schemas/javaxcdi/_AVckwBt1EeWb19NaJQmczQ/0",
						"http:///schemas/javaxinject/_AgglQBt1EeWb19NaJQmczQ/0",
						"http:///schemas/javaxinterceptor/_ArMLQBt1EeWb19NaJQmczQ/0",
						"http:///schemas/javaxvalidation/_8SEO4Bt0EeWb19NaJQmczQ/0",
						"http:///schemas/javaxws/_w3yHcBt1EeWb19NaJQmczQ/0",
						"http:///schemas/jaxb/_4tU80Bt0EeWb19NaJQmczQ/0",
						"http:///schemas/jaxrs/_oZrcQOg8EeSj2uQUDGM5Yw/1",
						"http:///schemas/jbossarquillian/_8c_FcBt0EeWb19NaJQmczQ/0",
						"http:///schemas/jdk7/_3-fg4Bt0EeWb19NaJQmczQ/0",
						"http:///schemas/jpa2/_4co1cBt0EeWb19NaJQmczQ/0",
						"http:///schemas/junit/_1Ka2wBt0EeWb19NaJQmczQ/0",
						"http:///schemas/objectify/_5G_aABt0EeWb19NaJQmczQ/0",
						"http:///schemas/springbundle/_8AogoBt0EeWb19NaJQmczQ/0",
						"http:///schemas/strutsbundlenolibrary/_AGTVgBt1EeWb19NaJQmczQ/0"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		ResourceSet resourceSet = new ResourceSetImpl();
		init(resourceSet);

		for (String uri : uprProfiles) {
			EObject eObj = resourceSet.getEObject(
					getEPackageNsURIToProfileLocationMap().get(uri), true);
			assertThat(eObj, instanceOf(Profile.class));
		}
	}

}
