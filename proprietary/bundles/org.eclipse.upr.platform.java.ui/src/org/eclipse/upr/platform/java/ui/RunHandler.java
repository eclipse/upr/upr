/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.upr.platform.java.cm2up.ResourceBasedCodeModel2UMLProfile;
import org.eclipse.upr.platform.java.cm2up.util.CodeModel2UMLProfileUtil;
import org.eclipse.upr.platform.java.code2cm.JavaDiscoverer;
import org.eclipse.upr.platform.java.jumpcfg.util.JConfigurationUtil;

public class RunHandler extends AbstractHandler {

	private final static String JUMP_CONSOLE = "Jump";
	private final static String TARGET_VIEW_ID = "org.eclipse.jdt.ui.PackageExplorer";

	private MessageConsole console;

	public RunHandler() {
		try {
			this.console = this.findConsole(RunHandler.JUMP_CONSOLE);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) {
		try {
			this.showConsole(event);
			this.clearConsole();

			IJavaProject project = this.getSelectedProject(event);

			MessageConsoleStream outStream = console.newMessageStream();
			outStream.println("Generating UML Profile for "
					+ project.getElementName());

			Resource codeModel = JavaDiscoverer.INSTANCE.runDiscovery(project);

			ResourceBasedCodeModel2UMLProfile runner = new ResourceBasedCodeModel2UMLProfile();
			runner.setUmlProfilePath(project
					.getPath()
					.append(project.getElementName().concat(
							CodeModel2UMLProfileUtil.UML_PROFILE_PATH_SEGMENT)).toString());
			runner.setTraceModelPath(project
					.getPath()
					.append(project.getElementName().concat(
							CodeModel2UMLProfileUtil.TRACE_PATH_SEGMENT)).toString());
			runner.loadModels(codeModel,
					JConfigurationUtil.getDefaultConfiguration(),
					CodeModel2UMLProfileUtil.JP_Model_Path,
					CodeModel2UMLProfileUtil.JPT_Model_Path,
					CodeModel2UMLProfileUtil.MC_Model_Path,
					CodeModel2UMLProfileUtil.UPT_Model_Path,
					CodeModel2UMLProfileUtil.EPT_Model_Path);
			runner.doCodeModel2UMLProfile(new NullProgressMonitor());
			runner.saveUMLProfileModel();

			outStream.println("Done ;)");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return null;
	}

	private IJavaProject getSelectedProject(ExecutionEvent event)
			throws ExecutionException, CoreException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		ISelectionService service = window.getSelectionService();
		IStructuredSelection structureSelection = (IStructuredSelection) service
				.getSelection(RunHandler.TARGET_VIEW_ID);

		// check if it is a IJavaProject and return it
		Object selection = structureSelection.getFirstElement();
		if (selection instanceof IJavaProject) {
			return (IJavaProject) selection;
		}
		throw new RuntimeException("No project selected");
	}

	private MessageConsole findConsole(String name) {
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();

		// search existing consoles
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++) {
			if (name.equals(existing[i].getName())) {
				return (MessageConsole) existing[i];
			}
		}

		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(name, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		conMan.showConsoleView(myConsole);
		return myConsole;
	}

	private void clearConsole() {
		this.console.clearConsole();
	}

	private void showConsole(ExecutionEvent event) throws PartInitException,
			ExecutionException {
		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		IWorkbenchPage page = window.getActivePage();
		String id = IConsoleConstants.ID_CONSOLE_VIEW;
		IConsoleView view = (IConsoleView) page.showView(id);
		view.display(this.console);
	}

}
