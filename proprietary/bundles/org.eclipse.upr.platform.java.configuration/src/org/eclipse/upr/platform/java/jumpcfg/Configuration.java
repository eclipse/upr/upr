/**
 */
package org.eclipse.upr.platform.java.jumpcfg;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.upr.platform.java.jumpcfg.Configuration#getConfigurationParameters <em>Configuration Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#getConfiguration()
 * @model
 * @generated
 */
public interface Configuration extends EObject {
	/**
	 * Returns the value of the '<em><b>Configuration Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration Parameters</em>' containment reference list.
	 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#getConfiguration_ConfigurationParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConfigurationParameter> getConfigurationParameters();

} // Configuration
