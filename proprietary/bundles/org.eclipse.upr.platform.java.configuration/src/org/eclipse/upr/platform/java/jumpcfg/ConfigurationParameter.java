/**
 */
package org.eclipse.upr.platform.java.jumpcfg;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#getConfigurationParameter()
 * @model
 * @generated
 */
public interface ConfigurationParameter extends EObject {
} // ConfigurationParameter
