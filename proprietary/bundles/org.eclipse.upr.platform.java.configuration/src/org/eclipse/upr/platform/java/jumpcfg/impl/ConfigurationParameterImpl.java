/**
 */
package org.eclipse.upr.platform.java.jumpcfg.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ConfigurationParameterImpl extends MinimalEObjectImpl.Container implements ConfigurationParameter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConfigurationPackage.Literals.CONFIGURATION_PARAMETER;
	}

} //ConfigurationParameterImpl
