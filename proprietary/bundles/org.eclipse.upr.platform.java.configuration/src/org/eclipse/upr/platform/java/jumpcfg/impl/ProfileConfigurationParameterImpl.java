/**
 */
package org.eclipse.upr.platform.java.jumpcfg.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage;
import org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Profile Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.upr.platform.java.jumpcfg.impl.ProfileConfigurationParameterImpl#getRepeatingStereotypes <em>Repeating Stereotypes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ProfileConfigurationParameterImpl extends ConfigurationParameterImpl implements ProfileConfigurationParameter {
	/**
	 * The default value of the '{@link #getRepeatingStereotypes() <em>Repeating Stereotypes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeatingStereotypes()
	 * @generated
	 * @ordered
	 */
	protected static final RepeatingStereotypesSolution REPEATING_STEREOTYPES_EDEFAULT = RepeatingStereotypesSolution.COMPOSITION;

	/**
	 * The cached value of the '{@link #getRepeatingStereotypes() <em>Repeating Stereotypes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepeatingStereotypes()
	 * @generated
	 * @ordered
	 */
	protected RepeatingStereotypesSolution repeatingStereotypes = REPEATING_STEREOTYPES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProfileConfigurationParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConfigurationPackage.Literals.PROFILE_CONFIGURATION_PARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RepeatingStereotypesSolution getRepeatingStereotypes() {
		return repeatingStereotypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepeatingStereotypes(RepeatingStereotypesSolution newRepeatingStereotypes) {
		RepeatingStereotypesSolution oldRepeatingStereotypes = repeatingStereotypes;
		repeatingStereotypes = newRepeatingStereotypes == null ? REPEATING_STEREOTYPES_EDEFAULT : newRepeatingStereotypes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES, oldRepeatingStereotypes, repeatingStereotypes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES:
				return getRepeatingStereotypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES:
				setRepeatingStereotypes((RepeatingStereotypesSolution)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES:
				setRepeatingStereotypes(REPEATING_STEREOTYPES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES:
				return repeatingStereotypes != REPEATING_STEREOTYPES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (repeatingStereotypes: ");
		result.append(repeatingStereotypes);
		result.append(')');
		return result.toString();
	}

} //ProfileConfigurationParameterImpl
