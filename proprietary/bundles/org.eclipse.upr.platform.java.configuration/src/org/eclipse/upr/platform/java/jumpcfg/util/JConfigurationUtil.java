/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.jumpcfg.util;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.upr.platform.java.jumpcfg.Configuration;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationFactory;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage;
import org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution;

public class JConfigurationUtil {
	
	private static Resource DEFAULT_CONFIGURATION = null;
	
	/**
	 * Produces a default configuration.
	 * 
	 * @return Jump configuration
	 */
	public static Resource getDefaultConfiguration() {
		if(DEFAULT_CONFIGURATION == null) {
			DEFAULT_CONFIGURATION = createDefaultConfiguration();
		} 
		return DEFAULT_CONFIGURATION;
	}
	
	private static Resource createDefaultConfiguration() {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
			.put(JConfigurationPackage.eNS_PREFIX, new XMIResourceFactoryImpl());
		URI uri = URI.createFileURI(new File("./jump-default.jcfg").getAbsolutePath());
		Resource resource = resourceSet.createResource(uri);
		Configuration configuration = JConfigurationFactory.eINSTANCE.createConfiguration();
		ProfileConfigurationParameter parameter = JConfigurationFactory.eINSTANCE.createProfileConfigurationParameter();
		parameter.setRepeatingStereotypes(RepeatingStereotypesSolution.COMPOSITION);
		configuration.getConfigurationParameters().add(parameter);
		resource.getContents().add(configuration);
		return resource;
	}

}
