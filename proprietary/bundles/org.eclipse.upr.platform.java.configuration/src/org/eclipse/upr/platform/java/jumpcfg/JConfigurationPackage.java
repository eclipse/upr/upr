/**
 */
package org.eclipse.upr.platform.java.jumpcfg;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationFactory
 * @model kind="package"
 * @generated
 */
public interface JConfigurationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "jumpcfg";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://org.eclipse.upr.platform.java.configuration/0.9";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "jcfg";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JConfigurationPackage eINSTANCE = org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationImpl
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 0;

	/**
	 * The feature id for the '<em><b>Configuration Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__CONFIGURATION_PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationParameterImpl <em>Configuration Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationParameterImpl
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getConfigurationParameter()
	 * @generated
	 */
	int CONFIGURATION_PARAMETER = 1;

	/**
	 * The number of structural features of the '<em>Configuration Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_PARAMETER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Configuration Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_PARAMETER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ProfileConfigurationParameterImpl <em>Profile Configuration Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ProfileConfigurationParameterImpl
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getProfileConfigurationParameter()
	 * @generated
	 */
	int PROFILE_CONFIGURATION_PARAMETER = 2;

	/**
	 * The feature id for the '<em><b>Repeating Stereotypes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES = CONFIGURATION_PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Profile Configuration Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_CONFIGURATION_PARAMETER_FEATURE_COUNT = CONFIGURATION_PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Profile Configuration Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROFILE_CONFIGURATION_PARAMETER_OPERATION_COUNT = CONFIGURATION_PARAMETER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution <em>Repeating Stereotypes Solution</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution
	 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getRepeatingStereotypesSolution()
	 * @generated
	 */
	int REPEATING_STEREOTYPES_SOLUTION = 3;


	/**
	 * Returns the meta object for class '{@link org.eclipse.upr.platform.java.jumpcfg.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.upr.platform.java.jumpcfg.Configuration#getConfigurationParameters <em>Configuration Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configuration Parameters</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.Configuration#getConfigurationParameters()
	 * @see #getConfiguration()
	 * @generated
	 */
	EReference getConfiguration_ConfigurationParameters();

	/**
	 * Returns the meta object for class '{@link org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter <em>Configuration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration Parameter</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter
	 * @generated
	 */
	EClass getConfigurationParameter();

	/**
	 * Returns the meta object for class '{@link org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter <em>Profile Configuration Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Profile Configuration Parameter</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter
	 * @generated
	 */
	EClass getProfileConfigurationParameter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter#getRepeatingStereotypes <em>Repeating Stereotypes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repeating Stereotypes</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter#getRepeatingStereotypes()
	 * @see #getProfileConfigurationParameter()
	 * @generated
	 */
	EAttribute getProfileConfigurationParameter_RepeatingStereotypes();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution <em>Repeating Stereotypes Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Repeating Stereotypes Solution</em>'.
	 * @see org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution
	 * @generated
	 */
	EEnum getRepeatingStereotypesSolution();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JConfigurationFactory getJConfigurationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationImpl
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '<em><b>Configuration Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFIGURATION__CONFIGURATION_PARAMETERS = eINSTANCE.getConfiguration_ConfigurationParameters();

		/**
		 * The meta object literal for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationParameterImpl <em>Configuration Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationParameterImpl
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getConfigurationParameter()
		 * @generated
		 */
		EClass CONFIGURATION_PARAMETER = eINSTANCE.getConfigurationParameter();

		/**
		 * The meta object literal for the '{@link org.eclipse.upr.platform.java.jumpcfg.impl.ProfileConfigurationParameterImpl <em>Profile Configuration Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.ProfileConfigurationParameterImpl
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getProfileConfigurationParameter()
		 * @generated
		 */
		EClass PROFILE_CONFIGURATION_PARAMETER = eINSTANCE.getProfileConfigurationParameter();

		/**
		 * The meta object literal for the '<em><b>Repeating Stereotypes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES = eINSTANCE.getProfileConfigurationParameter_RepeatingStereotypes();

		/**
		 * The meta object literal for the '{@link org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution <em>Repeating Stereotypes Solution</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution
		 * @see org.eclipse.upr.platform.java.jumpcfg.impl.JConfigurationPackageImpl#getRepeatingStereotypesSolution()
		 * @generated
		 */
		EEnum REPEATING_STEREOTYPES_SOLUTION = eINSTANCE.getRepeatingStereotypesSolution();

	}

} //JConfigurationPackage
