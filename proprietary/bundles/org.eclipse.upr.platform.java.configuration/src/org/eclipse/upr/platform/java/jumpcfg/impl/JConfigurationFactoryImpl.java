/**
 */
package org.eclipse.upr.platform.java.jumpcfg.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.upr.platform.java.jumpcfg.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JConfigurationFactoryImpl extends EFactoryImpl implements JConfigurationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JConfigurationFactory init() {
		try {
			JConfigurationFactory theJConfigurationFactory = (JConfigurationFactory)EPackage.Registry.INSTANCE.getEFactory(JConfigurationPackage.eNS_URI);
			if (theJConfigurationFactory != null) {
				return theJConfigurationFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JConfigurationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JConfigurationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JConfigurationPackage.CONFIGURATION: return createConfiguration();
			case JConfigurationPackage.CONFIGURATION_PARAMETER: return createConfigurationParameter();
			case JConfigurationPackage.PROFILE_CONFIGURATION_PARAMETER: return createProfileConfigurationParameter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case JConfigurationPackage.REPEATING_STEREOTYPES_SOLUTION:
				return createRepeatingStereotypesSolutionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case JConfigurationPackage.REPEATING_STEREOTYPES_SOLUTION:
				return convertRepeatingStereotypesSolutionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration createConfiguration() {
		ConfigurationImpl configuration = new ConfigurationImpl();
		return configuration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConfigurationParameter createConfigurationParameter() {
		ConfigurationParameterImpl configurationParameter = new ConfigurationParameterImpl();
		return configurationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProfileConfigurationParameter createProfileConfigurationParameter() {
		ProfileConfigurationParameterImpl profileConfigurationParameter = new ProfileConfigurationParameterImpl();
		return profileConfigurationParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RepeatingStereotypesSolution createRepeatingStereotypesSolutionFromString(EDataType eDataType, String initialValue) {
		RepeatingStereotypesSolution result = RepeatingStereotypesSolution.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRepeatingStereotypesSolutionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JConfigurationPackage getJConfigurationPackage() {
		return (JConfigurationPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JConfigurationPackage getPackage() {
		return JConfigurationPackage.eINSTANCE;
	}

} //JConfigurationFactoryImpl
