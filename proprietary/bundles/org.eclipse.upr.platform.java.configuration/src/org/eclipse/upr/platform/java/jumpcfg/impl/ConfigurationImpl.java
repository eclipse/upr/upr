/**
 */
package org.eclipse.upr.platform.java.jumpcfg.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.upr.platform.java.jumpcfg.Configuration;
import org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.eclipse.upr.platform.java.jumpcfg.impl.ConfigurationImpl#getConfigurationParameters <em>Configuration Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConfigurationImpl extends MinimalEObjectImpl.Container implements Configuration {
	/**
	 * The cached value of the '{@link #getConfigurationParameters() <em>Configuration Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigurationParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ConfigurationParameter> configurationParameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JConfigurationPackage.Literals.CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConfigurationParameter> getConfigurationParameters() {
		if (configurationParameters == null) {
			configurationParameters = new EObjectContainmentEList<ConfigurationParameter>(ConfigurationParameter.class, this, JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS);
		}
		return configurationParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS:
				return ((InternalEList<?>)getConfigurationParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS:
				return getConfigurationParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS:
				getConfigurationParameters().clear();
				getConfigurationParameters().addAll((Collection<? extends ConfigurationParameter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS:
				getConfigurationParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JConfigurationPackage.CONFIGURATION__CONFIGURATION_PARAMETERS:
				return configurationParameters != null && !configurationParameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ConfigurationImpl
