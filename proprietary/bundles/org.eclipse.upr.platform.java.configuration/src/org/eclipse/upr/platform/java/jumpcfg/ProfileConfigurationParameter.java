/**
 */
package org.eclipse.upr.platform.java.jumpcfg;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Profile Configuration Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter#getRepeatingStereotypes <em>Repeating Stereotypes</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#getProfileConfigurationParameter()
 * @model
 * @generated
 */
public interface ProfileConfigurationParameter extends ConfigurationParameter {
	/**
	 * Returns the value of the '<em><b>Repeating Stereotypes</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repeating Stereotypes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repeating Stereotypes</em>' attribute.
	 * @see org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution
	 * @see #setRepeatingStereotypes(RepeatingStereotypesSolution)
	 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#getProfileConfigurationParameter_RepeatingStereotypes()
	 * @model
	 * @generated
	 */
	RepeatingStereotypesSolution getRepeatingStereotypes();

	/**
	 * Sets the value of the '{@link org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter#getRepeatingStereotypes <em>Repeating Stereotypes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repeating Stereotypes</em>' attribute.
	 * @see org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution
	 * @see #getRepeatingStereotypes()
	 * @generated
	 */
	void setRepeatingStereotypes(RepeatingStereotypesSolution value);

} // ProfileConfigurationParameter
