/**
 */
package org.eclipse.upr.platform.java.jumpcfg.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.upr.platform.java.jumpcfg.Configuration;
import org.eclipse.upr.platform.java.jumpcfg.ConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationFactory;
import org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage;
import org.eclipse.upr.platform.java.jumpcfg.ProfileConfigurationParameter;
import org.eclipse.upr.platform.java.jumpcfg.RepeatingStereotypesSolution;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JConfigurationPackageImpl extends EPackageImpl implements JConfigurationPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass profileConfigurationParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum repeatingStereotypesSolutionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.upr.platform.java.jumpcfg.JConfigurationPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JConfigurationPackageImpl() {
		super(eNS_URI, JConfigurationFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JConfigurationPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static JConfigurationPackage init() {
		if (isInited) return (JConfigurationPackage)EPackage.Registry.INSTANCE.getEPackage(JConfigurationPackage.eNS_URI);

		// Obtain or create and register package
		JConfigurationPackageImpl theJConfigurationPackage = (JConfigurationPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JConfigurationPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JConfigurationPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theJConfigurationPackage.createPackageContents();

		// Initialize created meta-data
		theJConfigurationPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theJConfigurationPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JConfigurationPackage.eNS_URI, theJConfigurationPackage);
		return theJConfigurationPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfiguration() {
		return configurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConfiguration_ConfigurationParameters() {
		return (EReference)configurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfigurationParameter() {
		return configurationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProfileConfigurationParameter() {
		return profileConfigurationParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProfileConfigurationParameter_RepeatingStereotypes() {
		return (EAttribute)profileConfigurationParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRepeatingStereotypesSolution() {
		return repeatingStereotypesSolutionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JConfigurationFactory getJConfigurationFactory() {
		return (JConfigurationFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		configurationEClass = createEClass(CONFIGURATION);
		createEReference(configurationEClass, CONFIGURATION__CONFIGURATION_PARAMETERS);

		configurationParameterEClass = createEClass(CONFIGURATION_PARAMETER);

		profileConfigurationParameterEClass = createEClass(PROFILE_CONFIGURATION_PARAMETER);
		createEAttribute(profileConfigurationParameterEClass, PROFILE_CONFIGURATION_PARAMETER__REPEATING_STEREOTYPES);

		// Create enums
		repeatingStereotypesSolutionEEnum = createEEnum(REPEATING_STEREOTYPES_SOLUTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		profileConfigurationParameterEClass.getESuperTypes().add(this.getConfigurationParameter());

		// Initialize classes, features, and operations; add parameters
		initEClass(configurationEClass, Configuration.class, "Configuration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConfiguration_ConfigurationParameters(), this.getConfigurationParameter(), null, "configurationParameters", null, 0, -1, Configuration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(configurationParameterEClass, ConfigurationParameter.class, "ConfigurationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(profileConfigurationParameterEClass, ProfileConfigurationParameter.class, "ProfileConfigurationParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProfileConfigurationParameter_RepeatingStereotypes(), this.getRepeatingStereotypesSolution(), "repeatingStereotypes", null, 0, 1, ProfileConfigurationParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(repeatingStereotypesSolutionEEnum, RepeatingStereotypesSolution.class, "RepeatingStereotypesSolution");
		addEEnumLiteral(repeatingStereotypesSolutionEEnum, RepeatingStereotypesSolution.COMPOSITION);
		addEEnumLiteral(repeatingStereotypesSolutionEEnum, RepeatingStereotypesSolution.EMULATION);
		addEEnumLiteral(repeatingStereotypesSolutionEEnum, RepeatingStereotypesSolution.NATIVE);

		// Create resource
		createResource(eNS_URI);
	}

} //JConfigurationPackageImpl
