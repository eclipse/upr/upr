/*******************************************************************************
 * Copyright (c) 2015 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/

package org.eclipse.upr.platform.java.cm2up.util;

public class CodeModel2UMLProfileUtil {
	
	/** default input for the UML profile discovery transformation */
	public static final String JP_Model_Path = "pathmap://JAVA_PROFILE/javaProfile.uml"; 
	public static final String JPT_Model_Path = "pathmap://UML_LIBRARIES/JavaPrimitiveTypes.library.uml"; 
	public static final String MC_Model_Path = "pathmap://UML_METAMODELS/UML.metamodel.uml";
	public static final String UPT_Model_Path = "pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml";
	public static final String EPT_Model_Path = "pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.uml";
	public static final String UML_PROFILE_PATH_SEGMENT = "_profile.profile.uml";
	public static final String TRACE_PATH_SEGMENT = "_profile.trace.xmi";

}
