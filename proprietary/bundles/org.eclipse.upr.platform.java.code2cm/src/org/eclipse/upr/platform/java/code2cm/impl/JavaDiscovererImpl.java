/*******************************************************************************
 * Copyright (c) 2013 Vienna University of Technology.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Alexander Bergmayr (Vienna University of Technology) - initial API and implementation
 *
 * Initially developed in the context of ARTIST EU project www.artist-project.eu
 *******************************************************************************/
package org.eclipse.upr.platform.java.code2cm.impl;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.modisco.infra.discovery.core.exception.DiscoveryException;
import org.eclipse.modisco.java.discoverer.DiscoverJavaModelFromJavaProject;
import org.eclipse.upr.platform.java.code2cm.JavaDiscoverer;

/**
 * @author Alexander Bergmayr
 *
 */
public class JavaDiscovererImpl implements JavaDiscoverer {
	
	/**
	 * 
	 */
	private static boolean IS_INTIALIZED = false;
	
	/**
	 * 
	 */
	private JavaDiscovererImpl() {	}
	
	/**
	 * 
	 * @return the {@link JavaDiscoverer}
	 */
	public static JavaDiscoverer init() {
		if(!IS_INTIALIZED) {
			IS_INTIALIZED = true;
			return new JavaDiscovererImpl();
		} 
		return JavaDiscoverer.INSTANCE;
	}

	/** (non-Javadoc) 
	 * @see org.eclipse.upr.platform.java.code2cm.JavaDiscoverer#runDiscovery(java.lang.String)
	 */
	@Override
	public Resource runDiscovery(String projectName, IWorkspace workspace) throws DiscoveryException, CoreException {
		IJavaProject javaProject = getJavaProject(projectName, workspace);
		return runDiscovery(javaProject);
	}
	
	/** (non-Javadoc) 
	 * @see org.eclipse.upr.platform.java.code2cm.JavaDiscoverer#runDiscovery(org.eclipse.jdt.core.IJavaProject)
	 */
	@Override
	public Resource runDiscovery(IJavaProject project) throws JavaModelException, DiscoveryException {
		project.open(new NullProgressMonitor());
		DiscoverJavaModelFromJavaProject javaDiscoverer = new DiscoverJavaModelFromJavaProject();
		javaDiscoverer.setDeepAnalysis(false);
		javaDiscoverer.setSerializeTarget(false);
		javaDiscoverer.discoverElement(project, new NullProgressMonitor());
		Resource javaResource = javaDiscoverer.getTargetModel();
		return javaResource;
	}
	
	private IJavaProject getJavaProject(String projectName, IWorkspace workspace) {
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		return (IJavaProject) myWorkspaceRoot.getProject(projectName);
	}
	
}
